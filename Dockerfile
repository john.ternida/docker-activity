
# Use an official Node.js runtime as a parent image for the first stage
FROM node:14-alpine AS builder

# Set the working directory to /app
WORKDIR /app

# Copy package.json and package-lock.json to /app
COPY package*.json ./

# Install app dependencies
RUN npm install

# Copy the rest of the application code to /app
COPY . .

# Build the app for production
RUN npm run build --prod

# Use a minimal Nginx image as a parent image for the second stage
FROM nginx:stable-alpine

# Copy the production build output to /usr/share/nginx/html
COPY --from=builder /app/dist/marvel-heroes /usr/share/nginx/html

# Expose port to the host machine
EXPOSE 4400

# Start the Nginx server
CMD ["nginx", "-g", "daemon off;"]
